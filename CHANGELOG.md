# Changelog

All notable changes to `Chart.wire` will be documented in this file

## 1.0.0 - 2021-01-08

- initial release
