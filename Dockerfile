#
# PHP Dependencies
#
FROM composer:latest as vendor

COPY database/ database/

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --ignore-platform-reqs \
    --no-dev \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist

#
# Frontend
#
FROM node:latest as frontend

RUN mkdir -p /app/public

COPY package.json package-lock.json tailwind.config.js webpack.mix.js /app/
COPY resources/ /app/resources/

WORKDIR /app

RUN npm install && npm run prod

#
# Application
#
FROM php:7.4-fpm

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

# Install dependencies
RUN apt-get update && apt-get install -y \
        dnsutils \
        git \
        jpegoptim \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libzip-dev \
        optipng \
        pngquant \
        unzip \
        zip \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) \
        bcmath \
        exif \
        fileinfo \
        gd \
        pcntl \
        pdo_mysql \
        zip \
    # redis
    && pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis \
    # opcache
    && docker-php-ext-enable opcache \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# Install composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Switch back to dialog for any ad-hoc use of apt-get
ENV DEBIAN_FRONTEND=

# Setup application source
RUN mkdir -p /srv
COPY . /srv

COPY --from=vendor /app/vendor/ /srv/vendor/
COPY --from=frontend /app/public/js/ /srv/public/js/
COPY --from=frontend /app/public/css/ /srv/public/css/
COPY --from=frontend /app/public/images/ /srv/public/images/
COPY --from=frontend /app/public/mix-manifest.json /srv/public/mix-manifest.json

RUN chown -R www-data:www-data /srv

WORKDIR /srv
