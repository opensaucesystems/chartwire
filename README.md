## Introduction

Chart.wire are simple charts for use with [Laravel Livewire](https://laravel-livewire.com/).

- [Requirements](#requirements-)
- [Installation](#installation-)
- [Usage](#usage-)
- [Changelog](#changelog-)
- [Contributing](#contributing-)
- [Security](#security-)
- [License](#license-)

## Requirements [^](#introduction)

This package requires the following packages/libraries:

- [Alpine.js](https://github.com/alpinejs/alpine/)
- [Chart.js](https://www.chartjs.org/)
- [Laravel Livewire](https://laravel-livewire.com/)

## Installation [^](#introduction)

You can install the package via composer:

```bash
composer require opensaucesystems/chartwire
```

## Usage [^](#introduction)

Chart.wire supports out of the box the following types of charts

- Line Chart
- Bar Chart
- Doughnut Chart
- Pie Chart
- Area Chart
- Sparkline Chart

Each one comes with its own "model" class that allows you to define the chart's data and render behavior.

- Line Chart uses LineChartModel
- Bar Chart uses BarChartModel
- Doughnut Chart uses DoughnutChartModel
- Pie Chart uses PieChartModel
- Area Chart uses AreaChartModel
- Sparkline Chart uses SparklineChartModel

For example, to render a line chart, we create an instance of LineChartModel and pass it to the Livewire Line Chart component:

```php
$colors = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(54, 162, 235, 0.6)',
    'border' => 'rgba(54, 162, 235, 0.6)',
    'pointBackground' => 'rgba(54, 162, 235, 0.6)',
]);

$lineChartModel = (new \Opensaucesystems\Chartwire\Models\LineChartModel())
    ->addDataToDataset('Populations 2020', 1405544000, 'China', $colors)
    ->addDataToDataset('Populations 2020', 1370279686, 'India', $colors)
    ->addDataToDataset('Populations 2020', 330736378, 'United States', $colors)
    ->addDataToDataset('Populations 2020', 269603400, 'Indonesia', $colors)
    ->addDataToDataset('Populations 2020', 220892331, 'Pakistan', $colors)
    ->addDataToDataset('Populations 2020', 212404659, 'Brazil', $colors)
    ->addDataToDataset('Populations 2020', 206139587, 'Nigeria', $colors);
```
```html
<livewire:line-chartwire :line-chart-model="$lineChartModel" />
```

## Chart Model Methods [^](#introduction)

### Common methods

These methods are common to all chart models

| Method | Arguments | Description |
|----|----|----|
| addDataToDataset | _string_ **$datasetName** name of the dataset<br>*string&#124;int&#124;float* **$value** data value<br>*string* **$label** data label<br>*Opensaucesystems\Chartwire\Values\ColorValue* **$color** color of data on chart<br>*null&#124;string* **$type** chart type<br>*array* **$extras** extra chart options | Add data to chart dataset |
| reactiveKey | | returns a string based on its data |
| setOptions | *array* **$options** chart.js options | This will override any of the options set by the model methods as well as the default options. See [Chart.js docs](https://www.chartjs.org/docs/latest/)  |

<br>

### Legend methods

The following chart types support these methods:
- Line Chart
- Bar Chart
- Doughnut Chart
- Pie Chart
- Area Chart

| Method | Arguments | Description |
|----|----|----|
| withLegend | | Show legend |
| withoutLegend | | Hide legend |
| legendPositionTop | | Set legend position to top |
| legendPositionLeft | | Set legend position to left |
| legendPositionRight | | Set legend position to right |
| legendPositionBottom | | Set legend position to bottom |
| legendHorizontallyAlignedLeft | | Align legend horizontal left |
| legendHorizontallyAlignedCenter | | Align legend horizontal center |
| legendHorizontallyAlignedRight | | Align legend horizontal right |

<br>

### Axis methods

The following chart types support these methods:
- Line Chart
- Bar Chart
- Doughnut Chart
- Pie Chart
- Area Chart

| Method | Arguments | Description |
|----|----|----|
| withoutXAxis | | Hide x-axis |
| withXAxis | | Show x-axis |
| withoutYAxis | | Hide y-axis |
| withYAxis | | Show y-axis |

<br>

### Title methods

The following chart types support these methods:
- Line Chart
- Bar Chart
- Doughnut Chart
- Pie Chart
- Area Chart

| Method | Arguments | Description |
|----|----|----|
| withTitle | | Show title |
| withoutTitle | | Hide title |
| setTitleText | *string* **$text** | Set chart title |
| titlePositionTop | | Set title position to top |
| titlePositionLeft | | Set title position to left |
| titlePositionRight | | Set title position to right |
| titlePositionBottom | | Set title position to bottom |

<br>

### Event methods

The following chart types support these methods:
- Line Chart
- Bar Chart
- Doughnut Chart
- Pie Chart
- Area Chart

| Method | Arguments | Description |
|----|----|----|
| withOnClickEventName | *string* **$onClickEventName** | Livewire event name that will be fired when chart data is clicked  |

When an event of type `mouseup` or `click` is triggered on the chart, then Livewire will emit an event with the name of the argument passed into this method. It will also pass the label and value that was clicked.

```php
$lineChartModel = (new \Opensaucesystems\Chartwire\Models\LineChartModel())
    ->withOnClickEventName('onMyLineChartClicked');
```

Now you may register this event in another components $listeners property:

```php
class ShowData extends Component
{
    public $label;
    public $value;

    protected $listeners = ['onMyLineChartClicked' => 'handleMyLineChartClicked'];

    public function handleMyLineChartClicked(array $data)
    {
        $this->label = $data['label'];
        $this->value = $data['value'];
    }
}
```

<br>

### Stacked methods

The following chart types support these methods:
- Line Chart
- Bar Chart
- Area Chart

| Method | Arguments | Description |
|----|----|----|
| isStacked | *bool* **$stacked = true** | Stacked charts can be used to show how one data is made up of a number of smaller pieces.  |

<br>

## Sparkline charts [^](#introduction)

Sparkline charts are just a normal *Line Chart* with the default options hiding the legend, title, axes, tooltips etc.

To set the height and width of the *Sparline Chart* just use CSS.

This examples use [TailwindCSS](https://tailwindcss.com):

```php
$colors = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(54, 162, 235, 0.6)',
    'border' => 'rgba(54, 162, 235, 0.6)',
])

$sparklineChartModel = (new \Opensaucesystems\Chartwire\Models\SparklineChartModel())
        ->addDataToDataset('Populations 2020', 1405544000, 'China', $colors)
        ->addDataToDataset('Populations 2020', 1370279686, 'India', $colors)
        ->addDataToDataset('Populations 2020', 330736378, 'United States', $colors)
        ->addDataToDataset('Populations 2020', 269603400, 'Indonesia', $colors)
        ->addDataToDataset('Populations 2020', 220892331, 'Pakistan', $colors)
        ->addDataToDataset('Populations 2020', 212404659, 'Brazil', $colors)
        ->addDataToDataset('Populations 2020', 206139587, 'Nigeria', $colors);
```

```html
<div class="h-10 w-20">
    <livewire:sparkline-chartwire :sparkline-chart-model="$sparklineChartModel" />
</div>
```

<br>

## Colors [^](#introduction)

Each dataset requires a *ColorValue* object.

The *ColorValue* instance requires a border color. The background and pointBackground is optional.

The color can be simple a hexadecimal, RGB, or their color names.
```php
$colors = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'border' => 'rgba(54, 162, 235, 0.6)',
])
```

Since [Chart.js](https://chartjs.org) supports a [CanvasGradient](https://developer.mozilla.org/en-US/docs/Web/API/CanvasGradient) object, the ColorValue supports a *LinearGradientValue* object that is converted into a JS CanvasGradient object.

```php
use Opensaucesystems\Chartwire\Values\ColorStopValue;
use Opensaucesystems\Chartwire\Values\LinearGradientValue;

$backgroundColorStops = [
    new ColorStopValue(['color' => 'rgba(54, 162, 235, 0.6)', 'offset' => 0]),
    new ColorStopValue(['color' => 'rgba(54, 162, 235, 0)', 'offset' => 1]),
];
$borderColorStops = [
    new ColorStopValue(['color' => '#80b6f4', 'offset' => 0]),
    new ColorStopValue(['color' => '#f49080', 'offset' => 1]),
];

$backgroundGradient = new LinearGradientValue([
    'x1' => 0,
    'y1' => 100,
    'x2' => 0,
    'y2' => 370,
    'colorStops' => $backgroundColorStops,
])
$borderGradient = new LinearGradientValue([
    'x1' => 500,
    'y1' => 0,
    'x2' => 100,
    'y2' => 0,
    'colorStops' => $borderColorStops,
]);
```

<br>

## Multiple Datasets [^](#introduction)

You can also add multiple datasets to a chart that will allow you to group data.

### Stacked

Stacked charts allows you to see that data is made up of a number of smaller pieces.

```php
$colors2010 = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(54, 162, 235, 0.6)',
    'border' => 'rgba(54, 162, 235, 1)',
]);
$colors2020 = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(37, 194, 160, 0.6)',
    'border' => 'rgba(37, 194, 160, 1)',
]);

$barChartModel = (new \Opensaucesystems\Chartwire\Models\BarChartModel())
    ->addDataToDataset('Populations 2010', 1339724852, 'China', $colors2010)
    ->addDataToDataset('Populations 2010', 1182105564, 'India', $colors2010)
    ->addDataToDataset('Populations 2010', 309349689, 'United States', $colors2010)
    ->addDataToDataset('Populations 2010', 237641326, 'Indonesia', $colors2010)
    ->addDataToDataset('Populations 2010', 173510000, 'Pakistan', $colors2010)
    ->addDataToDataset('Populations 2010', 193252604, 'Brazil', $colors2010)
    ->addDataToDataset('Populations 2010', 158258917, 'Nigeria', $colors2010)
    ->addDataToDataset('Populations 2020', 1405544000, 'China', $colors2020)
    ->addDataToDataset('Populations 2020', 1370279686, 'India', $colors2020)
    ->addDataToDataset('Populations 2020', 330736378, 'United States', $colors2020)
    ->addDataToDataset('Populations 2020', 269603400, 'Indonesia', $colors2020)
    ->addDataToDataset('Populations 2020', 220892331, 'Pakistan', $colors2020)
    ->addDataToDataset('Populations 2020', 212404659, 'Brazil', $colors2020)
    ->addDataToDataset('Populations 2020', 206139587, 'Nigeria', $colors2020);
```
```html
<livewire:bar-chartwire :bar-chart-model="$barChartModel" />
```

## Troubleshooting [^](#introduction)

Chart components must be placed inside a container with fixed height. This is because the chart will use all the given
vertical space. A fixed height is needed to render properly.

 ```blade
<div style="height: 32rem;">
    <livewire:livewire-column-chart .../>
</div>
 ```

>Note: if a fixed height is not given, the chart will grow vertically infinitely. That's not what we want, right?

## Testing [^](#introduction)

``` bash
composer test
```

## Changelog [^](#introduction)

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing [^](#introduction)

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security [^](#introduction)

If you discover any security related issues, please email ashley@opensauce.systems instead of using the issue tracker.

## Credits and Influences [^](#introduction)

- [Laravel](https://laravel.com/)
- [Laravel Livewire](https://laravel-livewire.com/docs/quickstart/)
- [Tailwind CSS](https://tailwindcss.com/)
- [AlpineJS](https://github.com/alpinejs/alpine)
- [livewire-charts by asantibanez](https://github.com/asantibanez/livewire-charts)
- [All Contributors](../../graphs/master)

## License [^](#introduction)

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
