import areaChartwire from './areaChartwire'
import barChartwire from './barChartwire'
import doughnutChartwire from './doughnutChartwire'
import lineChartwire from './lineChartwire'
import pieChartwire from './pieChartwire'
import sparklineChartwire from './sparklineChartwire'

window.areaChartwire = areaChartwire
window.barChartwire = barChartwire
window.doughnutChartwire = doughnutChartwire
window.lineChartwire = lineChartwire
window.pieChartwire = pieChartwire
window.sparklineChartwire = sparklineChartwire
