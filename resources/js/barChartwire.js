import Chart from 'chart.js'
import {getColors} from './helper'

export default () => {
    return {
        chart: null,
        drawChart($wire) {
            if (this.chart) {
                this.chart.destroy()
            }

            const data = $wire.get('barChartModel.data')
            const datasets = Object.keys(data).map(datasetName => {
                let colors = getColors(data[datasetName])
                let item = {
                    datasetName,
                    label: datasetName,
                    data: data[datasetName].map(item => item.value),
                    type: data[datasetName][0].type,
                    backgroundColor: Array.isArray(colors.backgroundColor) ? colors.backgroundColor[0] : colors.backgroundColor,
                    borderColor: Array.isArray(colors.borderColor) ? colors.borderColor[0] : colors.borderColor,
                    pointBackgroundColor: Array.isArray(colors.pointBackgroundColor) ? colors.pointBackgroundColor[0] : colors.pointBackgroundColor,
                }

                if (data[datasetName][0].type === 'line') {
                    item.fill = false
                }

                let extras = data[datasetName]
                    .map(extra => extra.extras)
                    .reduce((accumulativeExtra, currentExtra) => {
                        return Object.assign({}, currentExtra, accumulativeExtra)
                    })

                return { ...item, ...extras}
            })
            const onClickEventName = $wire.get('barChartModel.onClickEventName')
            const labels = data[datasets[0].datasetName].map(item => item.label)
            const legend = $wire.get('barChartModel.legend') || {}
            const stacked = $wire.get('barChartModel.stacked');
            const title = $wire.get('barChartModel.title')
            const xAxes = $wire.get('barChartModel.xAxis');
            const yAxes = $wire.get('barChartModel.yAxis');

            const defaultOptions = {
                legend,
                maintainAspectRatio: false,
                onClick: (event) => {
                    let firstPoint = this.chart.getElementAtEvent(event)[0];

                    if (!onClickEventName || !firstPoint) {
                        return
                    }

                    $wire.call('onClick', {
                        label: this.chart.data.labels[firstPoint._index],
                        value: this.chart.data.datasets[firstPoint._datasetIndex].data[firstPoint._index],
                        backgroundColor: this.chart.data.datasets[firstPoint._datasetIndex].backgroundColor[firstPoint._index],
                    })
                },
                scales: {
                    yAxes: [
                        {
                            display: yAxes.display,
                            stacked
                        }
                    ],
                    xAxes: [
                        {
                            display: xAxes.display,
                            stacked
                        }
                    ]
                },
                title,
            }

            const options = $wire.get('barChartModel.options')

            let config = {
                type: 'bar',
                data: {
                    labels,
                    datasets,
                },
                options: {...defaultOptions, ...options}
            }

            config = Object.assign({}, config, $wire.get('barChartModel.title'))

            this.chart = new Chart(this.$refs.container.getContext('2d'), config)
        },
    }
}
