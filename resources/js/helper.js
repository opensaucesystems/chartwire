const createLinearGradient = (gradient, canvas) => {
    let linearGradient = canvas.getContext('2d').createLinearGradient(gradient.x1, gradient.y1, gradient.x2, gradient.y2, canvas)

    gradient.colorStops.forEach(colorStop => {
        linearGradient.addColorStop(colorStop.offset, colorStop.color)
    });
    return linearGradient
}

export const getColors = (dataset, canvas) => {
    let backgroundGradient, borderGradient, pointBackgroundGradient
    let colors = dataset.map(item => {
        let color = {}

        if (typeof item.color.background === 'object') {
            backgroundGradient = createLinearGradient(item.color.background, canvas)
        }

        if (typeof item.color.border === 'object') {
            borderGradient = createLinearGradient(item.color.border, canvas)
        }

        if (typeof item.color.pointBackground === 'object' && item.color.pointBackground !== null) {
            pointBackgroundGradient = createLinearGradient(item.color.pointBackground, canvas)
        }

        if (typeof item.color.background === 'string') {
            color.background = item.color.background
        }

        if (typeof item.color.border === 'string') {
            color.border = item.color.border
        }

        if (typeof item.color.pointBackground === 'string' || item.color.pointBackground === null) {
            color.pointBackground = item.color.pointBackground
        }

        return color
    })

    return {
        backgroundColor: backgroundGradient ? backgroundGradient : colors.map(color => color.background),
        borderColor: borderGradient ? borderGradient : colors.map(color => color.border),
        pointBackgroundColor: pointBackgroundGradient ? pointBackgroundGradient : colors.map(color => color.pointBackground),
    }
}
