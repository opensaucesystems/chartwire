import Chart from 'chart.js'
import {getColors} from './helper'

export default () => {
    return {
        chart: null,
        drawChart($wire) {
            if (this.chart) {
                this.chart.destroy()
            }

            const data = $wire.get('lineChartModel.data')
            const datasets = Object.keys(data).map(datasetName => {
                let colors = getColors(data[datasetName])
                let item = {
                    datasetName,
                    label: datasetName,
                    data: data[datasetName].map(item => item.value),
                    fill: false,
                    type: data[datasetName][0].type,
                    backgroundColor: Array.isArray(colors.backgroundColor) ? colors.backgroundColor[0] : colors.backgroundColor,
                    borderColor: Array.isArray(colors.borderColor) ? colors.borderColor[0] : colors.borderColor,
                    pointBackgroundColor: Array.isArray(colors.pointBackgroundColor) ? colors.pointBackgroundColor[0] : colors.pointBackgroundColor,
                }

                let extras = data[datasetName]
                    .map(extra => extra.extras)
                    .reduce((accumulativeExtra, currentExtra) => {
                        return Object.assign({}, currentExtra, accumulativeExtra)
                    })

                return { ...item, ...extras}
            })
            const labels = data[datasets[0].datasetName].map(item => item.label)
            const legend = $wire.get('lineChartModel.legend') || {}
            const onClickEventName = $wire.get('lineChartModel.onClickEventName')
            const stacked = $wire.get('lineChartModel.stacked');
            const title = $wire.get('lineChartModel.title')
            const xAxes = $wire.get('lineChartModel.xAxis');
            const yAxes = $wire.get('lineChartModel.yAxis');

            const defaultOptions = {
                legend,
                maintainAspectRatio: false,
                onClick: (event) => {
                    let firstPoint = this.chart.getElementAtEvent(event)[0];

                    if (!onClickEventName || !firstPoint) {
                        return
                    }

                    $wire.call('onClick', {
                        label: this.chart.data.labels[firstPoint._index],
                        value: this.chart.data.datasets[firstPoint._datasetIndex].data[firstPoint._index],
                        backgroundColor: this.chart.data.datasets[firstPoint._datasetIndex].backgroundColor[firstPoint._index],
                    })
                },
                scales: {
                    yAxes: [
                        {
                            display: yAxes.display,
                            stacked
                        }
                    ],
                    xAxes: [
                        {
                            display: xAxes.display,
                            stacked
                        }
                    ]
                },
                title,
            }

            const options = $wire.get('lineChartModel.options')

            const config = {
                type: 'line',
                data: {
                    labels,
                    datasets,
                },
                options: {...defaultOptions, ...options}
            }

            this.chart = new Chart(this.$refs.container.getContext('2d'), config)
        },
    }
}
