import Chart from 'chart.js'
import {getColors} from './helper'

export default () => {
    return {
        chart: null,
        drawChart($wire) {
            if (this.chart) {
                this.chart.destroy()
            }

            const data = $wire.get('pieChartModel.data')
            const datasets = Object.keys(data).map(datasetName => {
                let item = {
                    datasetName,
                    label: datasetName,
                    data: data[datasetName].map(item => item.value),
                    ...getColors(data[datasetName])
                }

                let extras = data[datasetName]
                    .map(extra => extra.extras)
                    .reduce((accumulativeExtra, currentExtra) => {
                        return Object.assign({}, currentExtra, accumulativeExtra)
                    })

                return { ...item, ...extras}
            })
            const onClickEventName = $wire.get('pieChartModel.onClickEventName')
            const labels =  data[datasets[0].datasetName].map(item => item.label)
            const legend = $wire.get('pieChartModel.legend') || {}
            const title = $wire.get('pieChartModel.title')

            const defaultOptions = {
                legend,
                maintainAspectRatio: false,
                onClick: (event) => {
                    let firstPoint = this.chart.getElementAtEvent(event)[0];

                    if (!onClickEventName || !firstPoint) {
                        return
                    }

                    $wire.call('onClick', {
                        label: this.chart.data.labels[firstPoint._index],
                        value: this.chart.data.datasets[firstPoint._datasetIndex].data[firstPoint._index],
                        backgroundColor: this.chart.data.datasets[firstPoint._datasetIndex].backgroundColor[firstPoint._index],
                    })
                },
                scales: {
                    yAxes: [
                        {
                            display: false,
                        }
                    ],
                    xAxes: [
                        {
                            display: false,
                        }
                    ]
                },
                title,
            }

            const options = $wire.get('pieChartModel.options')

            const config = {
                type: 'pie',
                data: {
                    labels,
                    datasets,
                },
                options: {...defaultOptions, ...options}
            }

            this.chart = new Chart(this.$refs.container.getContext('2d'), config)
        },
    }
}
