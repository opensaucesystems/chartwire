import Chart from 'chart.js'
import {getColors} from './helper'

export default () => {
    return {
        chart: null,
        drawChart($wire) {
            if (this.chart) {
                this.chart.destroy()
            }

            const data = $wire.get('sparklineChartModel.data')
            const datasets = Object.keys(data).map(datasetName => {
                let colors = getColors(data[datasetName])
                let item = {
                    datasetName,
                    label: datasetName,
                    data: data[datasetName].map(item => item.value),
                    fill: false,
                    backgroundColor: Array.isArray(colors.backgroundColor) ? colors.backgroundColor[0] : colors.backgroundColor,
                    borderColor: Array.isArray(colors.borderColor) ? colors.borderColor[0] : colors.borderColor,
                    pointBackgroundColor: Array.isArray(colors.pointBackgroundColor) ? colors.pointBackgroundColor[0] : colors.pointBackgroundColor,
                }

                let extras = data[datasetName]
                    .map(extra => extra.extras)
                    .reduce((accumulativeExtra, currentExtra) => {
                        return Object.assign({}, currentExtra, accumulativeExtra)
                    })

                return { ...item, ...extras}
            })
            const labels = data[datasets[0].datasetName].map(item => item.label)

            const defaultOptions = {
                elements: {
                    line: {
                        borderWidth: 1
                    },
                    point: {
                        radius: 0
                    }
                },
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                        {
                            display: false
                        }
                    ],
                    xAxes: [
                        {
                            display: false
                        }
                    ]
                },
                tooltips: {
                    enabled: false
                },
            }

            const options = $wire.get('sparklineChartModel.options')

            const config = {
                type: 'line',
                data: {
                    labels,
                    datasets,
                },
                options: {...defaultOptions, ...options}
            }

            this.chart = new Chart(this.$refs.container.getContext('2d'), config)
        },
    }
}
