<div
    style="width: 100%; height: 100%;"
    x-data="{ ...lineChartwire() }"
    x-init="drawChart($wire)"
>
    <canvas wire:ignore x-ref="container" width="600" height="400"></canvas>
</div>
