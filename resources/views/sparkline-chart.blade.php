<div
    style="width: 100%; height: 100%;"
    x-data="{ ...sparklineChartwire() }"
    x-init="drawChart($wire)"
>
    <canvas wire:ignore x-ref="container" width="150" height="100"></canvas>
</div>
