<?php

namespace Opensaucesystems\Chartwire\Charts;

use Livewire\Component;
use Opensaucesystems\Chartwire\Charts\Traits\HasOnClick;
use Opensaucesystems\Chartwire\Models\AreaChartModel;

/**
 * Class AreaChart
 * @package Opensaucesystems\Chartwire\Charts
 */
class AreaChart extends Component
{
    use HasOnClick;

    /** @var array<string, mixed> */
    public $areaChartModel;

    public function mount(AreaChartModel $areaChartModel): void
    {
        $this->areaChartModel = $areaChartModel->toArray();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('chartwire::area-chart');
    }
}
