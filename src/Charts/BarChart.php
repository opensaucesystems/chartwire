<?php

namespace Opensaucesystems\Chartwire\Charts;

use Livewire\Component;
use Opensaucesystems\Chartwire\Charts\Traits\HasOnClick;
use Opensaucesystems\Chartwire\Models\BarChartModel;

/**
 * Class BarChart
 * @package Opensaucesystems\Chartwire\Charts
 */
class BarChart extends Component
{
    use HasOnClick;

    /** @var array<string, mixed> */
    public $barChartModel;

    public function mount(BarChartModel $barChartModel): void
    {
        $this->barChartModel = $barChartModel->toArray();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('chartwire::bar-chart');
    }
}
