<?php

namespace Opensaucesystems\Chartwire\Charts;

use Livewire\Component;
use Opensaucesystems\Chartwire\Models\Contracts\ChartModelInterface;

/**
 * Class AreaChart
 * @package Opensaucesystems\Chartwire\Charts
 */
class ChartComponent extends Component
{
    /** @var array<string, mixed> */
    public $chartModel;

    public function mount(ChartModelInterface $chartModel): void
    {
        $this->chartModel = $chartModel->toArray();
    }
}
