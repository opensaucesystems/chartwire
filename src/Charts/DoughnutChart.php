<?php

namespace Opensaucesystems\Chartwire\Charts;

use Livewire\Component;
use Opensaucesystems\Chartwire\Charts\Traits\HasOnClick;
use Opensaucesystems\Chartwire\Models\DoughnutChartModel;

/**
 * Class DoughnutChart
 * @package Opensaucesystems\Chartwire\Charts
 */
class DoughnutChart extends Component
{
    use HasOnClick;

    /** @var array<string, mixed> */
    public $doughnutChartModel;

    public function mount(DoughnutChartModel $doughnutChartModel): void
    {
        $this->doughnutChartModel = $doughnutChartModel->toArray();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('chartwire::doughnut-chart');
    }
}
