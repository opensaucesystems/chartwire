<?php

namespace Opensaucesystems\Chartwire\Charts;

use Livewire\Component;
use Opensaucesystems\Chartwire\Charts\Traits\HasOnClick;
use Opensaucesystems\Chartwire\Models\LineChartModel;

/**
 * Class LineChart
 * @package Opensaucesystems\Chartwire\Charts
 */
class LineChart extends Component
{
    use HasOnClick;

    /** @var array<string, mixed> */
    public $lineChartModel;

    public function mount(LineChartModel $lineChartModel): void
    {
        $this->lineChartModel = $lineChartModel->toArray();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('chartwire::line-chart');
    }
}
