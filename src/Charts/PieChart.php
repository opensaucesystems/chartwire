<?php

namespace Opensaucesystems\Chartwire\Charts;

use Livewire\Component;
use Opensaucesystems\Chartwire\Charts\Traits\HasOnClick;
use Opensaucesystems\Chartwire\Models\PieChartModel;

/**
 * Class PieChart
 * @package Opensaucesystems\Chartwire\Charts
 */
class PieChart extends Component
{
    use HasOnClick;

    /** @var array<string, mixed> */
    public $pieChartModel;

    public function mount(PieChartModel $pieChartModel): void
    {
        $this->pieChartModel = $pieChartModel->toArray();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('chartwire::pie-chart');
    }
}
