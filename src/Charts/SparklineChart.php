<?php

namespace Opensaucesystems\Chartwire\Charts;

use Livewire\Component;
use Opensaucesystems\Chartwire\Models\SparklineChartModel;

/**
 * Class SparklineChart
 * @package Opensaucesystems\Chartwire\Charts
 */
class SparklineChart extends Component
{
    /** @var array<string, mixed> */
    public $sparklineChartModel;

    public function mount(SparklineChartModel $sparklineChartModel): void
    {
        $this->sparklineChartModel = $sparklineChartModel->toArray();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('chartwire::sparkline-chart');
    }
}
