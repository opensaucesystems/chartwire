<?php

namespace Opensaucesystems\Chartwire\Charts\Traits;

use Exception;
use Illuminate\Support\Str;

trait HasOnClick
{
    /**
     * @param array<string, mixed> $data
     */
    public function onClick(array $data): void
    {
        $onClickEventName = data_get($this->{$this->getNameOfChartModel()}, 'onClickEventName');

        if ($onClickEventName === null) {
            return;
        }

        $this->emit($onClickEventName, $data);
    }

    public function getNameOfChartModel(): string
    {
        $propertyName = Str::of(Str::of(static::class)->explode('\\')->last())->append('Model')->camel()->__toString();

        if (! property_exists(static::class, $propertyName)) {
            throw new Exception("Class property $propertyName doesn't exist!");
        }

        return $propertyName;
    }
}
