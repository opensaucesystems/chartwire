<?php

namespace Opensaucesystems\Chartwire;

use Opensaucesystems\Chartwire\Models\AreaChartModel;
use Opensaucesystems\Chartwire\Models\BarChartModel;
use Opensaucesystems\Chartwire\Models\LineChartModel;
use Opensaucesystems\Chartwire\Models\PieChartModel;
use Opensaucesystems\Chartwire\Models\SparklineChartModel;

class Chartwire
{
    public function areaChartModel(): AreaChartModel
    {
        return new AreaChartModel();
    }

    public function barChartModel(): BarChartModel
    {
        return new BarChartModel();
    }

    public function lineChartModel(): LineChartModel
    {
        return new LineChartModel();
    }

    public function pieChartModel(): PieChartModel
    {
        return new PieChartModel();
    }

    public function sparklineChartModel(): SparklineChartModel
    {
        return new SparklineChartModel();
    }
}
