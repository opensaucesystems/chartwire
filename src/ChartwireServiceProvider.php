<?php

namespace Opensaucesystems\Chartwire;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Opensaucesystems\Chartwire\Charts\AreaChart;
use Opensaucesystems\Chartwire\Charts\BarChart;
use Opensaucesystems\Chartwire\Charts\DoughnutChart;
use Opensaucesystems\Chartwire\Charts\LineChart;
use Opensaucesystems\Chartwire\Charts\PieChart;
use Opensaucesystems\Chartwire\Charts\SparklineChart;
use Opensaucesystems\Chartwire\View\Components\ChartwireJs;

class ChartwireServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->registerViews();
        $this->registerPublishables();
        $this->registerComponents();
        $this->registerLivewireComponents();
    }

    public function register(): void
    {
        $this->app->bind('chartwire', Chartwire::class);
    }

    private function registerViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'chartwire');
    }

    private function registerPublishables(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/chartwire'),
            ], 'chartwire:views');

            $this->publishes([
                __DIR__.'/../resources/js' => resource_path('js/vendor/chartwire'),
            ], 'chartwire:scripts');

            $this->publishes([
                __DIR__.'/../dist' => public_path('vendor/chartwire'),
            ], 'chartwire:public');
        }
    }

    private function registerComponents(): void
    {
        Blade::component('chartwire-js', ChartwireJs::class);
    }

    private function registerLivewireComponents(): void
    {
        Livewire::component('area-chartwire', AreaChart::class);
        Livewire::component('bar-chartwire', BarChart::class);
        Livewire::component('doughnut-chartwire', DoughnutChart::class);
        Livewire::component('line-chartwire', LineChart::class);
        Livewire::component('pie-chartwire', PieChart::class);
        Livewire::component('sparkline-chartwire', SparklineChart::class);
    }
}
