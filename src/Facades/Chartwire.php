<?php

namespace Opensaucesystems\Chartwire\Facades;

use Illuminate\Support\Facades\Facade;
use Opensaucesystems\Chartwire\Models\AreaChartModel;
use Opensaucesystems\Chartwire\Models\BarChartModel;
use Opensaucesystems\Chartwire\Models\DoughnutChartModel;
use Opensaucesystems\Chartwire\Models\LineChartModel;
use Opensaucesystems\Chartwire\Models\PieChartModel;
use Opensaucesystems\Chartwire\Models\SparklineChartModel;

/**
 * Class Chartwire
 * @package Opensaucesystems\Chartwire\Facades
 * @method static AreaChartModel areaChartModel()
 * @method static BarChartModel barChartModel()
 * @method static DoughnutChartModel doughnutChartModel()
 * @method static LineChartModel lineChartModel()
 * @method static PieChartModel pieChartModel()
 * @method static SparklineChartModel sparklineChartModel()
 */
class Chartwire extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'chartwire';
    }
}
