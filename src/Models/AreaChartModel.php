<?php

namespace Opensaucesystems\Chartwire\Models;

use Opensaucesystems\Chartwire\Models\Contracts\ChartModelInterface;
use Opensaucesystems\Chartwire\Models\Traits\HasStacked;

/**
 * Class AreaChartModel
 * @package Opensaucesystems\Chartwire\Models
 */
class AreaChartModel extends BaseChartModel implements ChartModelInterface
{
    use HasStacked;

    public function __construct()
    {
        parent::__construct();

        $this->initStacked();

        $this->chartType = 'line';
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return array_merge(
            parent::toArray(),
            $this->stackedToArray(),
            [
                'data' => $this->data->toArray(),
            ]
        );
    }

    /**
     * @param array<string, mixed> $array
     */
    public function fromArray(array $array): void
    {
        parent::fromArray($array);

        $this->stackedFromArray($array);

        $this->data = collect(data_get($array, 'data', []));
    }
}
