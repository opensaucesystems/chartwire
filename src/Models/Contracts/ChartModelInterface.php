<?php

namespace Opensaucesystems\Chartwire\Models\Contracts;

use Opensaucesystems\Chartwire\Values\ColorValue;

interface ChartModelInterface
{
    /**
     * Add data to chart dataset
     *
     * @param string $datasetName name of the dataset
     * @param string|int|float $value data value
     * @param string $label data label
     * @param \Opensaucesystems\Chartwire\Values\ColorValue $color color of data on chart
     * @param null|string $type chart type
     * @param array<string, mixed> $extras extra chart options
     * @return static
     */
    public function addDataToDataset(
        string $datasetName,
        $value,
        string $label,
        ColorValue $color,
        ?string $type = null,
        array $extras = []
    ): self;

    public function reactiveKey(): string;

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array;

    /**
     * @param array<string, mixed> $array
     */
    public function fromArray(array $array): void;
}
