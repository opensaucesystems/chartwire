<?php

namespace Opensaucesystems\Chartwire\Models;

use Opensaucesystems\Chartwire\Models\Contracts\ChartModelInterface;

/**
 * Class PieChartModel
 * @package Opensaucesystems\Chartwire\Models
 */
class PieChartModel extends BaseChartModel implements ChartModelInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->chartType = 'pie';
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return array_merge(parent::toArray(), [
            'data' => $this->data->toArray(),
        ]);
    }

    /**
     * @param array<string, mixed> $array
     */
    public function fromArray(array $array): void
    {
        parent::fromArray($array);

        $this->data = collect(data_get($array, 'data', []));
    }
}
