<?php

namespace Opensaucesystems\Chartwire\Models;

use Illuminate\Support\Collection;
use Opensaucesystems\Chartwire\Models\Contracts\ChartModelInterface;
use Opensaucesystems\Chartwire\Models\Traits\HasOptions;
use Opensaucesystems\Chartwire\Models\Traits\HasReactiveKey;

/**
 * Class SparklineChartModel
 * @package Opensaucesystems\Chartwire\Models
 */
class SparklineChartModel implements ChartModelInterface
{
    use HasOptions;
    use HasReactiveKey;

    /** @var \Illuminate\Support\Collection<string, mixed> */
    public Collection $data;

    protected string $chartType;

    public function __construct()
    {
        $this->initOptions();

        $this->chartType = 'line';
        $this->data = collect();
    }

    /**
     * Add data to chart dataset
     *
     * @param string $datasetName name of the dataset
     * @param string|int|float $value data value
     * @param string $label data label
     * @param \Opensaucesystems\Chartwire\Values\ColorValue $color color of data on chart
     * @param null|string $type chart type
     * @param array<string, mixed> $extras extra chart options
     * @return static
     */
    public function addDataToDataset(
        string $datasetName,
        $value,
        string $label,
        $color,
        ?string $type = null,
        array $extras = []
    ): self {
        if (! $type) {
            $type = $this->chartType;
        }

        $data = [
            'color' => $color,
            'datasetName' => $datasetName,
            'label' => $label,
            'value' => $value,
            'type' => $type,
            'extras' => $extras,
        ];

        $dataset = $this->data->get($datasetName, collect());

        $dataset->push($data);

        $this->data->put($datasetName, $dataset);

        return $this;
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return array_merge(
            $this->optionsToArray(),
            ['data' => $this->data->toArray()]
        );
    }

    /**
     * @param array<string, mixed> $array
     */
    public function fromArray(array $array): void
    {
        $this->optionsFromArray($array);

        $this->data = collect(data_get($array, 'data', []));
    }
}
