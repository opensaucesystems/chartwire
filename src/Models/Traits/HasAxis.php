<?php

namespace Opensaucesystems\Chartwire\Models\Traits;

trait HasAxis
{
    /** @var array<string, mixed> */
    private array $xAxis;

    /** @var array<string, mixed> */
    private array $yAxis;

    protected function initAxis(): void
    {
        $this->xAxis = $this->defaultXAxis();
        $this->yAxis = $this->defaultYAxis();
    }

    /**
     * @return array<string, bool>
     */
    private function defaultXAxis(): array
    {
        return [
            'display' => true,
        ];
    }

    /**
     * @return array<string, bool>
     */
    private function defaultYAxis(): array
    {
        return [
            'display' => true,
        ];
    }

    /**
     * @return static
     */
    public function setXAxisVisibility(bool $visible): self
    {
        data_set($this->xAxis, 'display', $visible);

        return $this;
    }

    /**
     * @return static
     */
    public function setYAxisVisibility(bool $visible): self
    {
        data_set($this->yAxis, 'display', $visible);

        return $this;
    }

    /**
     * @return static
     */
    public function withoutXAxis(): self
    {
        return $this->setXAxisVisibility(false);
    }

    /**
     * @return static
     */
    public function withXAxis(): self
    {
        return $this->setXAxisVisibility(true);
    }

    /**
     * @return static
     */
    public function withoutYAxis(): self
    {
        return $this->setYAxisVisibility(false);
    }

    /**
     * @return static
     */
    public function withYAxis(): self
    {
        return $this->setYAxisVisibility(true);
    }

    /**
     * @return array<string, array>
     */
    protected function axisToArray(): array
    {
        return [
            'xAxis' => $this->xAxis,
            'yAxis' => $this->yAxis,
        ];
    }

    /**
     * @param array<string, mixed> $array
     */
    protected function axisFromArray(array $array): void
    {
        $this->xAxis = data_get($array, 'xAxis', $this->defaultXAxis());

        $this->yAxis = data_get($array, 'yAxis', $this->defaultYAxis());
    }
}
