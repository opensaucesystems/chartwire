<?php

namespace Opensaucesystems\Chartwire\Models\Traits;

trait HasLegend
{
    /** @var array<string, mixed> */
    private array $legend;

    public function initLegend(): void
    {
        $this->legend = $this->defaultLegend();
    }

    /**
     * @return array<string, mixed>
     */
    private function defaultLegend(): array
    {
        return [
            'display' => false,
            'position' => 'bottom',
            'align' => 'center',
        ];
    }

    public function setLegendVisibility(bool $visible): self
    {
        data_set($this->legend, 'display', $visible);

        return $this;
    }

    public function setLegendPosition(string $position): self
    {
        data_set($this->legend, 'position', $position);

        return $this;
    }

    public function setLegendHorizontalAlign(string $horizontalAlign): self
    {
        data_set($this->legend, 'align', $horizontalAlign);

        return $this;
    }

    public function withoutLegend(): self
    {
        return $this->setLegendVisibility(false);
    }

    public function withLegend(): self
    {
        return $this->setLegendVisibility(true);
    }

    public function legendPositionTop(): self
    {
        return $this->setLegendPosition('top');
    }

    public function legendPositionLeft(): self
    {
        return $this->setLegendPosition('left');
    }

    public function legendPositionRight(): self
    {
        return $this->setLegendPosition('right');
    }

    public function legendPositionBottom(): self
    {
        return $this->setLegendPosition('bottom');
    }

    public function legendHorizontallyAlignedLeft(): self
    {
        return $this->setLegendHorizontalAlign('left');
    }

    public function legendHorizontallyAlignedCenter(): self
    {
        return $this->setLegendHorizontalAlign('center');
    }

    public function legendHorizontallyAlignedRight(): self
    {
        return $this->setLegendHorizontalAlign('right');
    }

    /**
     * @return array<string, array>
     */
    protected function legendToArray(): array
    {
        return [
            'legend' => $this->legend,
        ];
    }

    /**
     * @param array<string, mixed> $array
     */
    protected function legendFromArray(array $array): void
    {
        $this->legend = data_get($array, 'legend', $this->defaultLegend());
    }
}
