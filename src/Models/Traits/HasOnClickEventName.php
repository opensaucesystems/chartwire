<?php


namespace Opensaucesystems\Chartwire\Models\Traits;

trait HasOnClickEventName
{
    public ?string $onClickEventName;

    public function withOnClickEventName(string $onClickEventName): self
    {
        $this->onClickEventName = $onClickEventName;

        return $this;
    }

    protected function initOnClickEventName(): void
    {
        $this->onClickEventName = $this->defaultOnClickEventName();
    }

    /**
     * @return null
     */
    private function defaultOnClickEventName()
    {
        return null;
    }

    /**
     * @return array<string, string|null>
     */
    protected function onClickEventNameToArray(): array
    {
        return [
            'onClickEventName' => $this->onClickEventName,
        ];
    }

    /**
     * @param array<string, mixed> $array
     */
    protected function onClickEventNameFromArray(array $array): void
    {
        $this->onClickEventName = data_get($array, 'onClickEventName', $this->defaultOnClickEventName());
    }
}
