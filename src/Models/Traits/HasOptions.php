<?php


namespace Opensaucesystems\Chartwire\Models\Traits;

trait HasOptions
{
    /** @var array<string, mixed> */
    private array $options;

    /**
     * @param array<string, mixed> $options
     */
    public function setOptions(array $options): self
    {
        $this->options = array_merge($this->options, $options);

        return $this;
    }

    protected function initOptions(): void
    {
        $this->options = $this->defaultOptions();
    }

    /**
     * @return array<string, mixed>
     */
    private function defaultOptions(): array
    {
        return [];
    }

    /**
     * @return array<string, array>
     */
    protected function optionsToArray(): array
    {
        return [
            'options' => $this->options,
        ];
    }

    /**
     * @param array<string, mixed> $array
     */
    protected function optionsFromArray(array $array): void
    {
        $this->options = data_get($array, 'options', $this->defaultOptions());
    }
}
