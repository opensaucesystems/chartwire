<?php


namespace Opensaucesystems\Chartwire\Models\Traits;

use Exception;

trait HasReactiveKey
{
    public function reactiveKey(): string
    {
        $dataString = json_encode($this->toArray());

        if (! $dataString) {
            throw new Exception("Error Processing reactiveKey: converting data to string failed");
        }

        return md5($dataString);
    }
}
