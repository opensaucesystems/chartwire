<?php


namespace Opensaucesystems\Chartwire\Models\Traits;

trait HasStacked
{
    private bool $stacked;

    public function isStacked(bool $stacked = true): self
    {
        $this->stacked = $stacked;

        return $this;
    }

    protected function initStacked(): void
    {
        $this->stacked = $this->defaultStacked();
    }

    private function defaultStacked(): bool
    {
        return false;
    }

    /**
     * @return array<string, bool>
     */
    protected function stackedToArray(): array
    {
        return [
            'stacked' => $this->stacked,
        ];
    }

    /**
     * @param array<string, mixed> $array
     */
    protected function stackedFromArray(array $array): void
    {
        $this->stacked = data_get($array, 'stacked', $this->defaultStacked());
    }
}
