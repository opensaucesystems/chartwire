<?php


namespace Opensaucesystems\Chartwire\Models\Traits;

trait HasTitle
{
    /** @var (bool|string|string|int|string|float|int|string|string)[] */
    private array $title;

    /**
     * @param (bool|string|string|int|string|float|int|string|string)[] $title
     * @return static
     */
    public function setTitle(array $title): self
    {
        $this->title = array_merge($this->title, $title);

        return $this;
    }

    public function setTitleText(string $text): self
    {
        data_set($this->title, 'text', $text);

        // $this->setTitleVisibility(true);

        return $this;
    }

    public function setTitlePosition(string $position): self
    {
        data_set($this->title, 'position', $position);

        return $this;
    }

    public function setTitleVisibility(bool $visible): self
    {
        data_set($this->title, 'display', $visible);

        return $this;
    }

    public function withoutTitle(): self
    {
        return $this->setTitleVisibility(false);
    }

    public function withTitle(): self
    {
        return $this->setTitleVisibility(true);
    }

    public function setTitleFontSize(int $fontSize): self
    {
        data_set($this->title, 'fontSize', $fontSize);

        return $this;
    }

    public function setTitleFontFamily(string $fontFamily): self
    {
        data_set($this->title, 'fontFamily', $fontFamily);

        return $this;
    }

    public function setTitleFontColor(string $fontColor): self
    {
        data_set($this->title, 'fontColor', $fontColor);

        return $this;
    }

    public function setTitlePaddingY(int $padding): self
    {
        data_set($this->title, 'padding', $padding);

        return $this;
    }

    public function titlePositionTop(): self
    {
        return $this->setTitlePosition('top');
    }

    public function titlePositionLeft(): self
    {
        return $this->setTitlePosition('left');
    }

    public function titlePositionRight(): self
    {
        return $this->setTitlePosition('right');
    }

    public function titlePositionBottom(): self
    {
        return $this->setTitlePosition('bottom');
    }

    protected function initTitle(): void
    {
        $this->title = $this->defaultTitle();
    }

    /**
     * @return array<string, mixed>
     */
    private function defaultTitle(): array
    {
        return [
            'display' => false,
            'fontColor' => '#000',
            'fontFamily' => "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
            'fontSize' => 12,
            'fontStyle' => 'bold',
            'lineHeight' => 1.2,
            'padding' => 10,
            'position' => 'top',
            'text' => '',
        ];
    }

    /**
     * @return array
     *
     * @psalm-return array{title: mixed}
     */
    protected function titleToArray(): array
    {
        return [
            'title' => $this->title,
        ];
    }

    /**
     * @param array<string, mixed> $array
     */
    protected function titleFromArray(array $array): void
    {
        $this->title = data_get($array, 'title', $this->defaultTitle());
    }
}
