<?php

namespace Opensaucesystems\Chartwire\Values;

use Spatie\DataTransferObject\DataTransferObject;

class ColorStopValue extends DataTransferObject
{
    public string $color;

    /** @var int|float */
    public $offset;
}
