<?php

namespace Opensaucesystems\Chartwire\Values;

use Spatie\DataTransferObject\DataTransferObject;

class ColorValue extends DataTransferObject
{
    /**
     * @var \Opensaucesystems\Chartwire\Values\LinearGradientValue|string|null
     */
    public $background;

    /**
     * @var \Opensaucesystems\Chartwire\Values\LinearGradientValue|string
     */
    public $border;

    /**
     * @var \Opensaucesystems\Chartwire\Values\LinearGradientValue|string|null
     */
    public $pointBackground;
}
