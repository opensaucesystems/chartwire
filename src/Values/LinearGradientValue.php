<?php

namespace Opensaucesystems\Chartwire\Values;

use Spatie\DataTransferObject\DataTransferObject;

class LinearGradientValue extends DataTransferObject
{
    /** @var \Opensaucesystems\Chartwire\Values\ColorStopValue[] */
    public $colorStops;

    public int $x1;
    public int $y1;
    public int $x2;
    public int $y2;
}
