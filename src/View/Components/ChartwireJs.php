<?php

namespace Opensaucesystems\Chartwire\View\Components;

use Illuminate\View\Component;

class ChartwireJs extends Component
{
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|\Closure|string
     */
    public function render()
    {
        return <<<blade
            <script src="/vendor/chartwire/app.js"></script>
        blade;
    }
}
