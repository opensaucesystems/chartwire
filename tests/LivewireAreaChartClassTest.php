<?php

namespace Tests;

use Livewire\Livewire;
use Opensaucesystems\Chartwire\Charts\AreaChart;

$colors = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(54, 162, 235, 0.6)',
    'border' => 'rgba(54, 162, 235, 0.6)',
]);

$areaChartModel = (new \Opensaucesystems\Chartwire\Models\AreaChartModel())
    ->addDataToDataset('Populations 2020', 1405544000, 'China', $colors);

it('can mount areaChart using the model', function () use ($areaChartModel) {
    Livewire::test(AreaChart::class, ['areaChartModel' => $areaChartModel])
        ->assertSet('areaChartModel', $areaChartModel->toArray());
});

it('can render areaChart', function () use ($areaChartModel) {
    $areaChart = Livewire::test(AreaChart::class, ['areaChartModel' => $areaChartModel]);

    $html = <<<HTML
        style="width: 100%; height: 100%;"
        x-data="{ ...areaChartwire() }"
        x-init="drawChart(\$wire)"
    >
        <canvas wire:ignore x-ref="container" width="600" height="400"></canvas>
    </div>
    HTML;

    $areaChart->assertSeeHtml($html);
});
