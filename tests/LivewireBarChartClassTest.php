<?php

namespace Tests;

use Livewire\Livewire;
use Opensaucesystems\Chartwire\Charts\BarChart;

$colors = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(54, 162, 235, 0.6)',
    'border' => 'rgba(54, 162, 235, 0.6)',
]);

$barChartModel = (new \Opensaucesystems\Chartwire\Models\BarChartModel())
    ->addDataToDataset('Populations 2020', 1405544000, 'China', $colors);

it('can mount barChart using the model', function () use ($barChartModel) {
    Livewire::test(BarChart::class, ['barChartModel' => $barChartModel])
        ->assertSet('barChartModel', $barChartModel->toArray());
});

it('can render barChart', function () use ($barChartModel) {
    $barChart = Livewire::test(BarChart::class, ['barChartModel' => $barChartModel]);

    $html = <<<HTML
        style="width: 100%; height: 100%;"
        x-data="{ ...barChartwire() }"
        x-init="drawChart(\$wire)"
    >
        <canvas wire:ignore x-ref="container" width="600" height="400"></canvas>
    </div>
    HTML;

    $barChart->assertSeeHtml($html);
});
