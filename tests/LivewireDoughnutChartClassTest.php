<?php

namespace Tests;

use Livewire\Livewire;
use Opensaucesystems\Chartwire\Charts\DoughnutChart;

$colors = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(54, 162, 235, 0.6)',
    'border' => 'rgba(54, 162, 235, 0.6)',
]);

$doughnutChartModel = (new \Opensaucesystems\Chartwire\Models\DoughnutChartModel())
    ->addDataToDataset('Populations 2020', 1405544000, 'China', $colors);

it('can mount doughnutChart using the model', function () use ($doughnutChartModel) {
    Livewire::test(DoughnutChart::class, ['doughnutChartModel' => $doughnutChartModel])
        ->assertSet('doughnutChartModel', $doughnutChartModel->toArray());
});

it('can render doughnutChart', function () use ($doughnutChartModel) {
    $doughnutChart = Livewire::test(DoughnutChart::class, ['doughnutChartModel' => $doughnutChartModel]);

    $doughnutChart->assertSeeHtml('x-data="{ ...doughnutChart');
    $doughnutChart->assertSeeHtml('x-init="drawChart($wire)"');
});
