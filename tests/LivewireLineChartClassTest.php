<?php

namespace Tests;

use Livewire\Livewire;
use Opensaucesystems\Chartwire\Charts\LineChart;

$colors = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(54, 162, 235, 0.6)',
    'border' => 'rgba(54, 162, 235, 0.6)',
]);

$lineChartModel = (new \Opensaucesystems\Chartwire\Models\LineChartModel())
    ->addDataToDataset('Populations 2020', 1405544000, 'China', $colors);

it('can mount lineChart using the model', function () use ($lineChartModel) {
    Livewire::test(LineChart::class, ['lineChartModel' => $lineChartModel])
        ->assertSet('lineChartModel', $lineChartModel->toArray());
});

it('can render lineChart', function () use ($lineChartModel) {
    $lineChart = Livewire::test(LineChart::class, ['lineChartModel' => $lineChartModel]);

    $html = <<<HTML
        style="width: 100%; height: 100%;"
        x-data="{ ...lineChartwire() }"
        x-init="drawChart(\$wire)"
    >
        <canvas wire:ignore x-ref="container" width="600" height="400"></canvas>
    </div>
    HTML;

    $lineChart->assertSeeHtml($html);
});
