<?php

namespace Tests;

use Livewire\Livewire;
use Opensaucesystems\Chartwire\Charts\PieChart;

$colors = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(54, 162, 235, 0.6)',
    'border' => 'rgba(54, 162, 235, 0.6)',
]);

$pieChartModel = (new \Opensaucesystems\Chartwire\Models\PieChartModel())
    ->addDataToDataset('Populations 2020', 1405544000, 'China', $colors);

it('can mount pieChart using the model', function () use ($pieChartModel) {
    Livewire::test(PieChart::class, ['pieChartModel' => $pieChartModel])
        ->assertSet('pieChartModel', $pieChartModel->toArray());
});

it('can render pieChart', function () use ($pieChartModel) {
    $pieChart = Livewire::test(PieChart::class, ['pieChartModel' => $pieChartModel]);

    $html = <<<HTML
        style="width: 100%; height: 100%;"
        x-data="{ ...pieChartwire() }"
        x-init="drawChart(\$wire)"
    >
        <canvas wire:ignore x-ref="container" width="600" height="400"></canvas>
    </div>
    HTML;

    $pieChart->assertSeeHtml($html);
});
