<?php

namespace Tests;

use Livewire\Livewire;
use Opensaucesystems\Chartwire\Charts\SparklineChart;

$colors = new \Opensaucesystems\Chartwire\Values\ColorValue([
    'background' => 'rgba(54, 162, 235, 0.6)',
    'border' => 'rgba(54, 162, 235, 0.6)',
]);

$sparklineChartModel = (new \Opensaucesystems\Chartwire\Models\SparklineChartModel())
    ->addDataToDataset('Populations 2020', 1405544000, 'China', $colors);

it('can mount sparklineChart using the model', function () use ($sparklineChartModel) {
    Livewire::test(SparklineChart::class, ['sparklineChartModel' => $sparklineChartModel])
        ->assertSet('sparklineChartModel', $sparklineChartModel->toArray());
});

it('can render sparklineChart', function () use ($sparklineChartModel) {
    $sparklineChart = Livewire::test(SparklineChart::class, ['sparklineChartModel' => $sparklineChartModel]);

    $html = <<<HTML
        style="width: 100%; height: 100%;"
        x-data="{ ...sparklineChartwire() }"
        x-init="drawChart(\$wire)"
    >
        <canvas wire:ignore x-ref="container" width="150" height="100"></canvas>
    </div>
    HTML;

    $sparklineChart->assertSeeHtml($html);
});
